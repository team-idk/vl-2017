<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<main class="container-fluid">
    <h1>Retseptikumi kaart</h1>
    <ul>
        <li><a href="//retseptikum.ervinoro.eu/">Retsepti otsing</a></li>
        <?php
        if ($this->session->has_userdata('id')) {
            echo '<li ><a href = "//retseptikum.ervinoro.eu/index.php/Retsept/lisa" > Retsepti lisamine </a > </li >';
            echo '<li><a href = "//retseptikum.ervinoro.eu/ostunimekiri" > Ostunimekiri</a></li>';
            echo '<ul><li><a href = "//retseptikum.ervinoro.eu/ostunimekiri/edit" > Ostunimekirja muutmine</a></li></ul>';
            echo '<li><a href = "//retseptikum.ervinoro.eu/konto" > Kasutaja konto andmed</a></li>';

        }
        ?>
        <li>Retseptid
            <ul>
                <?php
                $recipes = $this->recipe_model->get_all_recipes();
                foreach ($recipes as $recipe) {
                    $url = "//retseptikum.ervinoro.eu/index.php/retsept/detailid/" . $recipe['id'];
                    echo '<li><a href="' . $url . '">' . $recipe['title'] . '</a></li>';
                }
                ?>
            </ul>
        </li>
    </ul>
</main>
