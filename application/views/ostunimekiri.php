<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# Closes BODY and HTML
$this->load->helper('xml');
$this->load->helper('form');
?>

<main id="ostunimekiri" class="container-fluid">
    <h1>Ostunimekiri</h1>

    <div class="row">
        <div class="col-sm-6">

                <?php
                if ($this->session->has_userdata('id')){
                    if (sizeof($groceries) > 0) {
                        echo "<table class=\"table\" id=\"ostunimekiritable\">";
                        for ($x = 0; $x < sizeof($groceries); $x++) {
                            echo "<tr><th>";
                            echo $groceries[$x]->amount;
                            echo " ";
                            echo $groceries[$x]->unit;
                            echo "</th><td>";
                            echo $groceries[$x]->name;
                            echo "</td><td>";
                            echo $groceries[$x]->recipename;
                            echo "</td><td>";
                            echo "<a href=\"" . base_url() . "ostunimekiri/eemalda/" . $groceries[$x]->id . "\">Eemalda</a>";
                            echo "</td></tr>\n";
                        }
                        echo "</table>";



                        echo "<br/>";
                        echo form_open('ostunimekiri/edit');
                        echo "<div class=\"row\">\n <div class=\"col-sm-2\"><input  class=\"btn btn-success\" type=\"submit\" value=\"Muuda\" /></div></div>";
                        echo form_close();

                    }
                    else {
                        echo "<p>Nimekiri on tühi</p>";
                    }
                }


                ?>

        </div>

    </div>


</main>
