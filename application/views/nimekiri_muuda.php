<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# Closes BODY and HTML
$this->load->helper('form');
?>


<main id="nimekiri_muuda" class="container-fluid">
    <h1>Ostunimekiri</h1>

            <?php
            echo form_open('ostunimekiri/uuenda');

            echo "<table class=\"table\" id=\"ingredients\">";

            echo "\n<colgroup>\n
                       <col class=\"col-auto\" />\n
                       <col />\n
                    </colgroup>\n";
            for ($x = 0; $x < sizeof($groceries); $x++) {
                echo "\n<tr><td>\n
                                <span id=\"kogus".$x."_error\" class=\"control-group error\">" . form_error("kogus" . $x) . "</span>
                                <input name=\"kogus" . $x . "\" class=\"form-control\" type=\"number\" data-toggle=\"tooltip\" title=\"Mitu ühikut ainet on vaja\" value=\"";
                echo $groceries[$x]->amount;
                echo "\"/>\n
                                </td><td>\n
                                <span id=\"uhik".$x."_error\" class=\"control-group error\">" . form_error("ühik" . $x) . "</span>
                                <input maxlength=\"4\" name=\"ühik" . $x . "\" class=\"form-control\"  type=\"text\" data-toggle=\"tooltip\" title=\"Kasuta üheselt mõistetavaid ühikuid\" value=\"";
                echo $groceries[$x]->unit;
                echo "\"/>\n
                                </td><td>\n
                                <span id=\"aine".$x."_error\" class=\"control-group error\">" . form_error("aine" . $x) . "</span>
                                <input name=\"aine" . $x . "\" class=\"form-control\" type=\"text\" data-toggle=\"tooltip\" title=\"Palun jälgi õigekirja\" value=\"";
                echo $groceries[$x]->name;
                echo "\"/>\n</td><td>\n";
                echo $groceries[$x]->recipename;
                echo "\n</td></tr>\n";
                echo "<input type=\"hidden\" name=\"shoplistid".$x."\" value=\"". $groceries[$x]->id ."\"/>\n";
                echo '<input type="hidden" name="recipe'.$x.'" value="'. $groceries[$x]->recipe.'"/>';
            }
            echo "<input name=\"koostisosi\" type=\"hidden\" id=\"listipikkus\" value=\"" . sizeof($groceries) . "\" />";
            echo "</table>";
            echo "<div class=\"row\">\n <div class=\"col-sm-2\"><input  class=\"btn btn-success\" type=\"submit\" value=\"Salvesta\" /></div></div>";
            echo form_close();
            ?>

</main>
