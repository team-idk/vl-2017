<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# Closes BODY and HTML
$this->load->helper('xml');
$this->load->helper('form');
?>
<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="loginModalLabel">Sisselogimine pooleli</h4>
            </div>
            <div class="modal-body">
                Jätkamiseks palun kinnitage sisselogimine mõnest muust seadmest.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Loobu</button>
            </div>
        </div>
    </div>
</div>
<main id="container" class="container-fluid">
    <?php echo form_open('user/redirect', 'onsubmit="javascript:sendLoginRequest(); return false;"'); ?>
    <!-- login without id-card -->
    <div class="row">
        <div class="col-sm-12">
            <label for="idcode">Isikukood</label>
            <input class="form-control" type="text" id="idcode" name="idcode" data-toggle="tooltip"
                   title="Siit kaudu sisse logimiseks pead olema kusagil mujal juba id-kaardiga Retseptikumi sisse logitud"/>
            <br/>
            <input type="submit" value="Logi sisse"/>
        </div>
    </div>
    <?php echo form_close() ?>
    <form class="" id="backdoor" onsubmit="backDoor(); return false;"><input type="submit" value="Backdoor"/></form>
</main>


