<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# Closes BODY and HTML
$this->load->helper('xml');
$this->load->helper('form');
?>

<div id="result-list" class="container-fluid">
    <?php
    #displays search results as a list of links
    if (count($recipes) == 0){
        echo '<p class="search_result">Ei leidnud tulemusi</p>';

    }else {
    echo '<ul class="search_result">';

    foreach ($recipes as $recipe) {
        $url = "//retseptikum.ervinoro.eu/index.php/retsept/detailid/".$recipe['id'];
        echo '<li><a href="'.$url.'">' . $recipe['title'] . '</a></li>';
    }
    echo '</ul>';
    echo '<button class="search_result" id="show_more_btn">Rohkem tulemusi</button>';
    }
    ?>

</div>
