<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# Closes BODY and HTML
$this->load->helper('xml');
$this->load->helper('form');
?>

<main class="container-fluid">
    <!-- Name of the author and edit button -->
    <div class="row">
        <div class="col-sm-3 col-sm-offset-6">
            <div class="col-sm-12" >
                <h4> Autor: <?php echo $autor; ?></h4>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12 text-right">
                <?php if ($this->session->has_userdata('id')){
                    if($autorID == $this->session->id){
                        echo "<a class=\"btn btn-success\" href=\"" . base_url() . "index.php/Retsept/lisa/" . $id . "\" data-toggle=\"tooltip\" title=\"Muuda seda retspti\">Muuda</a>\n";
                        echo "<button class=\"btn btn-danger\" onclick=\"showDelete()\" data-toggle=\"tooltip\" title=\"Kustuta see retsept
                        \">Kustuta</button>\n";                       
                    }
                }
                ?>
            </div>
        </div>
        <div class="col-sm-6 col-sm-offset-6">
            <div class="col-sm-12">
                <div class="alert alert-danger" id="deleteDialogue" hidden="hidden">
                    <div class="row">
                        <div class="col-sm-12">
                            <strong>Kas sa tahad selle retsepti jäädavalt kustutada?</strong> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <a class="btn btn-info" href="<?php echo base_url() . "index.php/Retsept/kustuta/" . $id ?>" data-toggle="tooltip" title="Kinnita kustutamine">Jah</a> 
                            <button class="btn btn-info" onclick="hideDelete()" data-toggle="tooltip" title="Loobu kustutamisest">Ei</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row ">
        <!-- title, for how many, how long, ingredients -->
        <div class="col-sm-6">

            <div class="col-sm-12">
                <h2><?php echo $nimi; ?></h2>
            </div>


            <div class="col-sm-6">
                <h3>Mitmele: <?php echo $mitmele; ?></h3>
            </div>
            <div class="col-sm-6">
                <h3 data-toggle="tooltip" title="Kui kaua kulub toidu valmistamiseks autori hinnangul">Aeg: <?php echo $ajakulu; ?></h3>
            </div>

            <div class="col-sm-12">
                <h3>Sildid: <span class="h4"><?php echo implode(', ', $sildid); ?></span></h3>
            </div>

            <div class="col-sm-12">
                <h3>Koostisosad:</h3>
                <?php
                if (sizeof($koostisosad) > 0) {
                    echo "<table class=\"table\" id=\"ingredients\">";
                    echo "\n<colgroup>\n
                       <col class=\"col-auto\" />\n
                       <col />\n
                    </colgroup>\n";

                    //present all listed ingredients
                    for ($x = 0; $x < sizeof($koostisosad); $x++) {
                        echo "<tr><th>";
                        echo $koostisosad[$x]->amount;
                        echo " ";
                        echo $koostisosad[$x]->unit;
                        echo "</th>\n<td>";
                        echo $koostisosad[$x]->name;
                        echo "</td></tr>";
                    }
                    echo "</table>";
                }
                ?> 


                <!-- <button class="btn btn-success">Lisa nimekirja</button> -->
                <?php if ($this->session->has_userdata('id')){

                    if ($this->shoppinglist_model->inshoppinglist($this->session->id, $id)){
                        echo "<p>Lisatud ostunimekirja</p>";
                    }
                    else {
                        echo form_open('ostunimekiri/salvesta', array('id' => 'ostunimekiri_form'));
                        echo "\n";
                        echo "<input type=\"hidden\" name=\"recipe\" value=\"" . $id . "\"/>\n";
                        echo "<input name=\"koostisosi\" type=\"hidden\" id=\"koostisosi\" value=\"" . sizeof($koostisosad) . "\" />";
                        for ($x = 0; $x < sizeof($koostisosad); $x++) {
                            echo "<input type=\"hidden\" name=\"ühik" . $x . "\" value=\"" . $koostisosad[$x]->unit . "\"/>\n";
                            echo "<input type=\"hidden\" name=\"kogus" . $x . "\" value=\"" . $koostisosad[$x]->amount . "\"/>\n";
                            echo "<input type=\"hidden\" name=\"aine" . $x . "\" value=\"" . $koostisosad[$x]->name . "\"/>\n";

                        }


                        echo "<input type=\"submit\" class=\"btn btn-success\" value=\"Lisa ostunimekirja\" />\n";

                        echo form_close();
                    }
                }?>
            </div>
        </div>
        <!-- (placeholder) image  -->
        <div class="col-sm-6">
            <div class="col-sm-12" >
                <div class ="delay-image" data-image-src="<?php echo $pilt; ?>">
                </div>
            </div>
        </div>


        </div>
        <!-- description and details -->
        <div class="col-sm-12">
            <h4><?php echo $kirjeldus; ?></h4>
            <pre><?php echo $juhend; ?></pre>
        </div>



</main>

