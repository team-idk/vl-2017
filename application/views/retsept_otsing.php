<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# Closes BODY and HTML
$this->load->helper('xml');
$this->load->helper('form');
?>

<main id="container" class="container-fluid">
    <h1>Otsi retsept</h1>

    <?php echo form_open('retsept/sync_otsing', array('role' => "search", 'id' => 'search_form', 'method' => 'get', 'onsubmit' => 'return sendXHR(this)', 'class' => 'row col-sm-12')); ?>
    <!-- search recipes -->
        <label for="tags">Sildid</label>
        <input class="form-control" type="text" id="tags" name="tags" placeholder="nt Magustoit, Jäätis" data-toggle="tooltip" title="Komaga eraldatud nimekiri siltidest, mis võivad olla retseptile määratud" />
        <br />
        <label for="keywords">Võtmesõnad</label>
        <input class="form-control" type="text" id="keywords" name="keywords" data-toggle="tooltip" placeholder="nt kartulid võiga" title="Vabas vormis võtmesõnad, mis võivad leiduda retsepti kirjelduses" />
        <br />
        <input type="submit" class="btn btn-success" value="Otsi" />
    <?php echo form_close() ?>
</main>


