<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# Contains HEAD, opens HTML and BODY but does not close them
$this->load->helper('url');
$this->load->helper('html');

echo doctype('html5');
?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo $title; ?> - Retseptikum</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700i" rel="stylesheet"/>
    <?php echo link_tag('css/bootstrap.css'); ?>
    <?php echo link_tag('css/main.css'); ?>
    <script src="https://cdn.ravenjs.com/3.13.1/raven.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/notsuspicious.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>js/later.min.js"></script>
</head>
<body>
<!-- common header -->
<header class="navbar">
    <div class="navbar-header">
        <!-- logo -->
        <a href="<?php echo base_url(); ?>" class="navbar-brand">
            <svg xmlns="http://www.w3.org/2000/svg" height="3em" width="13em">
                <rect class="logo-bg" x="0" y="0" rx="0.5em" ry="0.5em" width="13em" height="3em" fill="#9D0000"></rect>
                <text class="logo" font-size="34" x="0.5em" y="1.2em" fill="#FFCCAA" stroke="#220000">Retseptikum</text>
            </svg>
        </a>
        <!-- hamburger menu -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav_menu" value="Ava menüü" aria-label="Ava menüü">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <!-- part of the navbar that turnes into burger on small windows -->
    <nav class="collapse navbar-collapse" id="nav_menu">
        <div class="container-fluid">
            <?php
            if ($this->session->has_userdata('id')) { //if logged in
                if ($title == "Retsepti otsing") {
                    $tosearch = "<a class=\"nav_current\" data-toggle=\"tooltip\" title=\"Te olete juba otsingu lehel\">Otsing</a>";
                } else {
                    $tosearch = "<a href=\"" . base_url() . "\"  data-toggle=\"tooltip\" title=\"Siit saab tagasi avalehele\">Otsing</a>";
                }
                if ($title == "Ostunimekiri") {
                    $tolist = "<a class=\"nav_current\" data-toggle=\"tooltip\" title=\"Te olete juba ostunimekirja lehel\">Ostunimekiri</a>";
                } else {
                    $tolist = "<a href=\"" . base_url() . "ostunimekiri\"  data-toggle=\"tooltip\" title=\"Siit saab ostunimekirja juurde\">Ostunimekiri</a>";
                }
                if ($title == "Retsepti lisamine") {
                    $tolisa = "<a class=\"nav_current\" data-toggle=\"tooltip\" title=\"Te olete juba retsepti lisamise lehel\">Lisa retsept</a>";
                } else {
                    $tolisa = "<a href=\"" . base_url() . "index.php/Retsept/lisa\"  data-toggle=\"tooltip\" title=\"Võimaldab koostada uue retsepti nullist\">Lisa retsept</a>";
                }
                if ($title == "Saidikaart") {
                    $tokaart = "<a class=\"nav_current\" data-toggle=\"tooltip\" title=\"Te olete saidikaardi lehel\">Saidikaart</a>";
                } else {
                    $tokaart = "<a href=\"" . base_url() . "kaart\"  data-toggle=\"tooltip\" title=\"Kuvab kaardi antud veebisaidi sisuga\">Saidikaart</a>";
                }
                if ($title == "Kasutaja konto") {
                    $tokonto = "<a class=\"nav_current\" data-toggle=\"tooltip\" title=\"Te olete konto lehel\">Minu konto</a>";
                } else {
                    $tokonto = "<a href=\"" . base_url() . "konto\"  data-toggle=\"tooltip\" title=\"Kuvab kasutaja konto\">Minu konto</a>";
                }
                echo ul(
                    array(
                        $tokaart,
                        $tokonto,
                        $tosearch,
                        $tolist,
                        //"<a href=\"#shopping-list\" id=\"shopping\" data-placement=\"bottom\">Ostunimekiri</a>",
                        $tolisa,
                        "<a href=\"/user/logout\" id=\"logout_button\">Logi välja</a>"
                    ),
                    array('class' => "nav navbar-nav navbar-right"));
            } else if ($title == "Kaug-logimine") {
                echo ul(
                    array(
                        "<a href=\"http:".base_url()."\" data-toggle=\"tooltip\" title=\"Teid suunatakse tagasi pealehele\">Loobu sisselogimisest</a>"
                    ),
                    array('class' => "nav navbar-nav navbar-right"));
            } else {
                if ($title == "Retsepti otsing") {
                    $tosearch = "<a class=\"nav_current\" data-toggle=\"tooltip\" title=\"Te olete juba otsingu lehel\">Otsing</a>";
                } else {
                    $tosearch = "<a href=\"" . base_url() . "\"  data-toggle=\"tooltip\" title=\"Siit saab tagasi avalehele\">Otsing</a>";
                }
                if ($title == "Saidikaart") {
                    $tokaart = "<a class=\"nav_current\" data-toggle=\"tooltip\" title=\"Te olete saidikaardi lehel\">Saidikaart</a>";
                } else {
                    $tokaart = "<a href=\"" . base_url() . "kaart\"  data-toggle=\"tooltip\" title=\"Kuvab kaardi antud veebisaidi sisuga\">Saidikaart</a>";
                }
                echo ul(
                    array(
                        $tokaart,
                        $tosearch,
                        "<a href=\"https:".current_url()."\" data-toggle=\"tooltip\" title=\"Logi sisse id-kaardiga või isikukoodiga\">Logi sisse</a>"
                    ),
                    array('class' => "nav navbar-nav navbar-right"));
            } ?>
        </div>
    </nav>
</header>

<!-- contents of shopping list -->
<!--aside role="complementary" id="shopping-list" class="hidden popover container">
    <button type="button" class="button btn-danger" onclick="hideShopping()">x</button>
</aside-->



