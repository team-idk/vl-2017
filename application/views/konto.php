<?php

defined('BASEPATH') OR exit('No direct script access allowed');
# Closes BODY and HTML
?>
<main id="konto" class="container-fluid">
    <h1>Kasutaja andmed</h1>

    <div class="row">
        <div class="col-sm-6">
            <h4> Nimi: <?php echo $nimi; ?></h4>
            <h4> Isikukood: <?php echo $isikukood; ?></h4>
        <?php
        if (sizeof($recipes)>0) {
            echo "<br/>";
            echo "<h4> Lisatud retseptid: </h4>";
            echo "<ul>";
            foreach ($recipes as $recipe) {
                $url = "//retseptikum.ervinoro.eu/index.php/retsept/detailid/" . $recipe['id'];
                echo '<li><a href="' . $url . '">' . $recipe['title'] . '</a></li>';
            }
            echo "</ul>";
        }

        ?>
        </div>
    </div>


</main>