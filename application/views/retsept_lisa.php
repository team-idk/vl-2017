<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# Closes BODY and HTML
$this->load->helper('form');
?>

<main class="container-fluid">
    <?php echo form_open_multipart('retsept/salvesta' . ($id ? '/'.$id : ''),array('enctype' => "multipart/form-data")); ?>
    <!-- Name of the author and save button -->
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <h4>Autor: <?php echo $autor ?></h4>
            </div>
            <div class="col-sm-2">
                <input  class="btn btn-success" type="submit" value="Salvesta" />
            </div>
        </div>
        <!-- title, for how many, how long, ingredients -->
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <label class="control-label">Pealkiri</label>
                        <span id="pealkiri_error" class="control-group error"><?php echo form_error('pealkiri'); ?></span>
                        <input name="pealkiri" value="<?php echo $nimi; ?>"
                               class="form-control" type="text" placeholder="Pealkiri" data-toggle="tooltip"
                               title="Anna toidule kirjeldav pealkiri" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label" >Mitmele</label>
                        <span id="mitmele_error" class="control-group error"><?php echo form_error('mitmele'); ?></span>
                        <input name="mitmele" value="<?php echo $mitmele; ?>"
                               class="form-control" type="number" data-toggle="tooltip"
                               title="Mitu portsjonit sellest retseptist saab"/>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" >Ajakulu</label>
                        <span id="ajakulu_error" class="control-group error"><?php echo form_error('ajakulu'); ?></span>
                        <input name="ajakulu" value="<?php echo $ajakulu; ?>"
                               class="form-control"
                               placeholder="hh:mm:ss" type="text" data-toggle="tooltip"
                               title="Kui kaua kulub tavaliselt selle söögi valmistamiseks? (hh:mm:ss)"/>
                    </div>
                </div>
                <div class="row">
                <div class="col-sm-12">
                    <input name="aineid" type="hidden" id="aineid" value="<?php echo sizeof($koostisosad)+1; ?>" />
                    <table class="table" id="ingredients">
                    <tr>
                        <th>
                            <label class="control-label">Kogus</label>
                        </th>
                        <th>
                            <label class="control-label">Ühik</label>
                        </th>
                        <th>
                           <label class="control-label"> Nimetus</label>
                        </th>
                    </tr>
                    <?php 
                        for ($x = 0; $x < sizeof($koostisosad); $x++) {
                            echo "\n<tr><td>\n
                                <input name=\"kogus" . $x . "\" class=\"form-control\" type=\"number\" data-toggle=\"tooltip\" title=\"Mitu ühikut ainet on vaja\" value=\"";
                            echo $koostisosad[$x]->amount;
                            echo "\"/>\n
                                </td><td>\n
                                <input maxlength=\"4\" name=\"ühik" . $x . "\" class=\"form-control\"  type=\"text\" data-toggle=\"tooltip\" title=\"Kasuta üheselt mõistetavaid ühikuid\" value=\"";
                            echo $koostisosad[$x]->unit;
                            echo "\"/>\n
                                </td><td>\n
                                <input name=\"aine" . $x . "\" class=\"form-control\" type=\"text\" data-toggle=\"tooltip\" title=\"Palun jälgi õigekirja\" value=\"";
                            echo $koostisosad[$x]->name;
                            echo "\"/>\n
                                </td></tr>\n";
                        }
                    ?> 
                    <tr> 
                        <td>
                            <span id="kogus0_error" class="control-group error"><?php echo form_error('kogus0'); ?></span>
                            <input name="kogus<?php echo sizeof($koostisosad).'"'; if (sizeof($koostisosad) == 0) echo ''; ?> class="form-control" type="number" min="0" data-toggle="tooltip" title="Mitu ühikut ainet on vaja"/>
                        </td>
                        <td>
                            <span id="ühik0_error" class="control-group error"><?php echo form_error('ühik0'); ?></span>
                            <input maxlength="4" name="ühik<?php echo sizeof($koostisosad).'"'; if (sizeof($koostisosad) == 0) echo ''; ?> class="form-control" type="text" data-toggle="tooltip" title="Kasuta üheselt mõistetavaid ühikuid" />
                        </td>
                        <td>
                            <span id="aine0_error" class="control-group error"><?php echo form_error('aine0'); ?></span>
                            <input name="aine<?php echo sizeof($koostisosad).'"'; if (sizeof($koostisosad) == 0) echo ''; ?> class="form-control" type="text" data-toggle="tooltip" title="Aine nimi. Palun jälgi õigekirja" />
                        </td>
                    </tr>
                </table>
                </div>
                </div>
                <div class="row">
                    <div class="col-sm-1">
                        <button type="button" class="button" onclick="addIngredient()" data-toggle="tooltip" title="Lisa aine">+</button>
                    </div>
                </div>
            </div>
            <!-- Place to upload image -->
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <label class="control-label">Pilt</label>
                    <span id="pilt_error" class="control-group error"><?php echo $pilt ?></span>
                    <input accept="image/*" type="file"  data-toggle="tooltip" title="Lisa ilus pilt selle retsepti tulemusest" name="image"/>
                    
                </div>
            </div>
        </div>
        <!-- description and directions -->
        <div class="row">
            <div class="col-sm-12">
                <h2><label class="control-label">Kirjeldus</label></h2>
                <span id="kirjeldus_error" class="control-group error"><?php echo form_error('kirjeldus'); ?></span>
                <textarea name="kirjeldus" class="form-control" rows="2" data-toggle="tooltip"
                          title="Lühike kirjeldus"
                ><?php echo $kirjeldus; ?></textarea>
                <h2><label class="control-label">Valmistamise juhend</label></h2>
                <span id="juhend_error" class="control-group error"><?php echo form_error('juhend'); ?></span>
                <textarea name="juhend" class="form-control" rows="5" data-toggle="tooltip"
                          title="Samm-sammult juhised sinu toidu valmistamiseks"
                ><?php echo $juhend; ?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <label class="control-label">Sildid</label>
                        <span id="sildid_error" class="control-group error"><?php echo form_error('sildid'); ?></span>
                <input name="sildid" class="form-control" type="text" data-toggle="tooltip" placeholder="lihtne, kiire"
                       title="Komadega eraldatud sildid. Palun jälgi õigekirja!" value="<?php echo $sildid; ?>" />
            </div>
        </div>
    </form>
</main>

