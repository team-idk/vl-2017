<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ostunimekiri extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        require_once 'push/vendor/sentry/sentry/lib/Raven/Autoloader.php';
        Raven_Autoloader::register();
        $client = new Raven_Client('https://45dceb073fe74671b17057201bec938c:fbb98949242641949a2985a52ee08d14@sentry.io/151789');
        $client->install();
    }

    public function index()
    {
        $this->load->model('user_model');
        $this->load->model('shoppinglist_model');
        $this->load->model('recipe_model');
        $this->user_model->auth();
        header("Content-Type: application/xhtml+xml; charset=utf-8");
        $this->load->view('navigatsioon', array('title' => 'Ostunimekiri'));
        $groceries = $this->shoppinglist_model->get_shoppinglist($this->session->id);
        $this->load->view('ostunimekiri', ['groceries' => $groceries]);
        $this->load->view('footer');


    }

    public function salvesta()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        if (!isset($_SERVER['HTTPS'])) {
            $this->load->helper('url');
            redirect(base_url('ostunimekiri/salvesta', 'https:'));
        }
        $this->load->model('user_model');
        $this->load->model('shoppinglist_model');
        $this->load->model('recipe_model');
        $this->user_model->auth();
        $ingredients = array();
        $i=0;
        while ($i<intval(set_value('koostisosi'))){
            $grocery = new stdClass();
            $grocery->name = html_escape(set_value('aine'.$i));
            $grocery->unit = html_escape(set_value('ühik'.$i));
            $grocery->amount = html_escape(set_value('kogus'.$i));
            array_push($ingredients, $grocery);
            $i+=1;
        };
        $list = array(
            'recipe' => html_escape(set_value('recipe')),
            'ingredients' => $ingredients,
            'user' => $this->session->id
        );
        $this->shoppinglist_model->store_recipe_to_shoppinglist($list);

        redirect(base_url('retsept/detailid/' . html_escape(set_value('recipe')) , 'https:'));

    }

    public function uuenda()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        if (!isset($_SERVER['HTTPS'])) {
            redirect(base_url('ostunimekiri/uuenda', 'https:'));
        }

        $this->load->model('user_model');
        $this->load->model('shoppinglist_model');
        $this->load->model('recipe_model');
        $this->user_model->auth();
        $ingredients = array();
        $i=0;
        while ($i<intval(set_value('koostisosi'))) {
            if(set_value('aine'.$i) == "" & set_value('ühik'.$i) == "" & set_value('kogus'.$i) == ""){
                $this->shoppinglist_model->removefromlist(html_escape(set_value('shoplistid'.$i)));
            } else {
                $this->form_validation->set_rules('ühik' . $i, 'ühik' . $i, 'required|max_length[4]');
                $this->form_validation->set_rules('kogus' . $i, 'kogus' . $i, 'required|integer|is_natural_no_zero');
                $this->form_validation->set_rules('aine' . $i, 'aine' . $i, 'required');
            }
            $i += 1;
        };
        $validation = $this->form_validation->run();
        $i=0;
        while ($i<intval(set_value('koostisosi'))) {
            $grocery = new stdClass();
            $grocery->name = html_escape(set_value('aine' . $i));
            $grocery->unit = html_escape(set_value('ühik' . $i));
            $grocery->amount = html_escape(set_value('kogus' . $i));
            $grocery->id = html_escape(set_value('shoplistid' . $i));
            $grocery->recipe = html_escape(set_value('recipe' . $i));
            $grocery->recipename = $this->recipe_model->get_recipename($grocery->recipe);
            array_push($ingredients, $grocery);
            $i += 1;
        }
        if ($validation == FALSE) {
            $this->edit($ingredients);
        } else {
            $list = array(
                'ingredients' => $ingredients
            );
            $this->shoppinglist_model->update_shoppinglist($list);
            redirect(base_url('ostunimekiri'), 'https:');
        }
    }

    public function edit($groceries = NULL){
        $this->load->model('user_model');
        $this->load->model('shoppinglist_model');
        $this->load->model('recipe_model');
        $this->user_model->auth();
        header("Content-Type: application/xhtml+xml; charset=utf-8");
        $this->load->view('navigatsioon', array('title' => 'Muuda nimekirja'));
        if ($groceries == NULL) {
            $groceries = $this->shoppinglist_model->get_shoppinglist($this->session->id);
        }
        $this->load->view('nimekiri_muuda',['groceries' => $groceries]);
        $this->load->view('footer');

    }

    public function eemalda($listid){
        if (!isset($_SERVER['HTTPS'])) {
            $this->load->helper('url');
            redirect(base_url('ostunimekiri/salvesta', 'https:'));
        }
        $this->load->helper('url');
        $this->load->model('user_model');
        $this->user_model->auth();
        $this->load->model('shoppinglist_model');
        $this->shoppinglist_model->removefromlist($listid);
        redirect(base_url('ostunimekiri') , 'https:');

    }
}