<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    function __construct() {
        parent::__construct();
        require_once 'push/vendor/sentry/sentry/lib/Raven/Autoloader.php';
        Raven_Autoloader::register();
        $client = new Raven_Client('https://45dceb073fe74671b17057201bec938c:fbb98949242641949a2985a52ee08d14@sentry.io/151789');
        $client->install();
    }

	public function login()
	{
        if (!isset($_SERVER['HTTPS'])) {
            $this->load->helper('url');
            redirect(base_url('retsept/lisa', 'https:'));
        }
        header("Content-Type: application/xhtml+xml; charset=utf-8");
        $this->load->view('navigatsioon', array('title' => 'Kaug-logimine'));
        $this->load->view('user_login');
        $this->load->view('footer');
    }

    public function logout()
    {
        $this->load->model('user_model');
        $this->user_model->deauth();
        $this->load->helper('url');
        redirect('http://retseptikum.ervinoro.eu/');
    }

    public function redirect() {
        $this->load->model('user_model');
        $this->user_model->auth();
        $this->load->helper('url');
        $togo = $this->session->pre_login_url;
        $this->session->unset_userdata('pre_login_url');
        redirect($togo);
    }

    public function backdoor() {
        $db = new \mysqli('localhost', 'codeigniter', '', 'vl_2017');
        // Authorize
        $id = "12345678900";
        $sessionId = $this->session->session_id;
        $query = $db->prepare("CALL update_session(?, ?)");
        $query->bind_param('ss', $sessionId, $id);
        $query->execute();
        $db->close();
        $this->redirect();
    }
}
