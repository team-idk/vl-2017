<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kaart extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        require_once 'push/vendor/sentry/sentry/lib/Raven/Autoloader.php';
        Raven_Autoloader::register();
        $client = new Raven_Client('https://45dceb073fe74671b17057201bec938c:fbb98949242641949a2985a52ee08d14@sentry.io/151789');
        $client->install();
    }

    public function index()
    {
        $this->load->model('user_model');
        $this->user_model->auth();
        $this->load->model('recipe_model');
        header("Content-Type: application/xhtml+xml; charset=utf-8");
        $this->load->view('navigatsioon', array('title' => 'Saidikaart'));
        $this->load->view('sitemap');
        $this->load->view('footer');


        }
}