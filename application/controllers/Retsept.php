<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retsept extends CI_Controller
{
    function __construct() {
        parent::__construct();
        require_once 'push/vendor/sentry/sentry/lib/Raven/Autoloader.php';
        Raven_Autoloader::register();
        $client = new Raven_Client('https://45dceb073fe74671b17057201bec938c:fbb98949242641949a2985a52ee08d14@sentry.io/151789');
        $client->install();
        $this->load->model('shoppinglist_model');
        $this->load->model('image_model');

    }

	public function index()
	{
        $this->load->model('user_model');
	    $this->user_model->auth();
        header("Content-Type: application/xhtml+xml; charset=utf-8");
        $this->load->view('navigatsioon', array('title' => 'Retsepti otsing'));
        $this->load->view('retsept_otsing');
        $this->load->view('footer');
    }

    public function detailid($id)
    {
        $this->load->model('user_model');
        $this->load->model('ingredient_model');
        $this->user_model->auth();
        header("Content-Type: application/xhtml+xml; charset=utf-8");

        $src = "https://68.media.tumblr.com/7e69d89eb549895a1919c414a0d39c9b/tumblr_omo0wkfrkZ1rfoc21o1_500.jpg";
        $this->load->model('image_model');
        $image = $this->image_model->get_image($id);
        if (sizeof($image) == 1) {
            $src = 'data: '.$image[0]['mime'].';base64,'.base64_encode($image[0]['image']);
        }

        $this->load->model('recipe_model');
        $recipe = $this->recipe_model->get_recipe($id);
        $this->load->view('navigatsioon', array('title' => $recipe['title']));
        $this->load->view('retsept_detailid',
            array(
                'id' => $id,
                'nimi' => $recipe['title'],
                'autor' => $this->user_model->get_user_name($recipe['author'])[0]->nimi,
                'autorID' => $recipe['author'],
                'ajakulu' => $recipe['prep'],
                'mitmele' => $recipe['servings'],
                'kirjeldus' => $recipe['description'],
                'juhend' => $recipe['directions'],
                'koostisosad' => $this->ingredient_model->get_ingredients($id),
                'sildid' => $recipe['tags'],
                'pilt' => $src
            ));
        $this->load->view('footer');            
    
    }

    public function kustuta($id) {
        $this->load->helper('url');
        if (!isset($_SERVER['HTTPS'])) {
            redirect(base_url('retsept/detailid/' . $id , 'http:'));
        }
        $this->load->model('user_model');
        $this->user_model->auth();
        $this->load->model('recipe_model');
        $recipe = $this->recipe_model->get_recipe($id);
        if($recipe['author'] === $this->session->id){
            $this->recipe_model->remove_recipe($id);
            redirect(base_url('','https:'));

        }else redirect(base_url('retsept/detailid/' . $id , 'https:'));


       }
    public function salvesta($id=null) {

        if (!isset($_SERVER['HTTPS'])) {
            $this->load->helper('url');
            redirect(base_url('retsept/lisa', 'https:'));
        }
        $this->load->model('user_model');
        $this->user_model->auth();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('pealkiri', 'Pealkiri', 'required');
        $this->form_validation->set_rules('mitmele', 'Mitmele', 'required|integer|is_natural_no_zero');
        $this->form_validation->set_rules('ajakulu', 'Ajakulu', 'required|regex_match[@\d{1,2}:\d{1,2}:\d{1,2}(\.\d{1,7})?@]',
            array('regex_match' => ' %s peab olema kujul HH:MM:SS'));
        $this->form_validation->set_rules('kirjeldus', 'Kirjeldus', 'required');
        $this->form_validation->set_rules('juhend', 'Juhend', 'required');
        $this->form_validation->set_rules('kogus0', 'Kogus0', 'required');
        $this->form_validation->set_rules('ühik0', 'Ühik0', 'required');
        $this->form_validation->set_rules('aine0', 'Aine0', 'required');
        $this->form_validation->set_rules('aineid', 'aineid', 'required|integer|is_natural_no_zero');


        if ($this->form_validation->run() == FALSE) {
            $this->lisa($id, TRUE);
        }
        else {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 100;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $this->load->library('upload', $config);
            $image_status = "";
            if (!empty($_FILES['image']['name'])) {
                if ( ! $this->upload->do_upload('image')) {
                    $image_status = $this->upload->display_errors();
                    $this->lisa($id, FALSE, $image_status);
                    return;
                } else {
                    $data = $this->upload->data();
                    $this->load->model('image_model');
                    $handle = fopen($data['full_path'], "r");
                    $contents = fread($handle, filesize($data['full_path']));
                    fclose($handle);
                }
            }
            $this->load->model('recipe_model');
            $koostisosad = array();
            $i=0;
            while ($i<intval(set_value('aineid'))){
                if (set_value('aine'.$i) == "" || set_value('ühik'.$i) == "" || set_value('kogus'.$i) == "") {
                    $i+=1;
                    continue;
                }
                $koostisosa = new stdClass();
                $koostisosa->name = html_escape(set_value('aine'.$i));
                $koostisosa->unit = html_escape(set_value('ühik'.$i));
                $koostisosa->amount = html_escape(set_value('kogus'.$i));
                array_push($koostisosad, $koostisosa);
                $i+=1;
            };
            $recipe = array(
                'id' => $id,
                'pealkiri' => html_escape(set_value('pealkiri')),
                'mitmele' => set_value('mitmele'),
                'ajakulu' => set_value('ajakulu'),
                'kirjeldus' => html_escape(set_value('kirjeldus')),
                'juhend' => html_escape(set_value('juhend')),
                'sildid' => html_escape(set_value('sildid')),
                'koostisosad' => $koostisosad
            );
            $id = $this->recipe_model->store($recipe);
            if (!empty($_FILES['image']['name'])) {
                $this->image_model->store_image($id, $contents, $data['file_type']);
            }
            $this->detailid($id);
        }
    }

    public function lisa($id=null, $nopopulate=FALSE, $image_status="")
    {
        if (!isset($_SERVER['HTTPS'])) {
            $this->load->helper('url');
            redirect(base_url('retsept/lisa', 'https:'));
        }
        $this->load->model('user_model');
        $this->user_model->auth();
        header("Content-Type: application/xhtml+xml; charset=utf-8");
        $this->load->view('navigatsioon', array('title' => 'Retsepti lisamine'));
        if (!function_exists('set_value')) {
            function set_value($field = null) {
                return '';
            }
        }
        if ($id != null && $nopopulate == FALSE){
            $this->load->model('recipe_model');
            $recipe = $this->recipe_model->get_recipe($id);
            if($recipe['author'] === $this->session->id){
                $this->load->model('ingredient_model');
                $this->load->view('retsept_lisa',
                array(
                    'id'=> $id,
                    'autor'=> $this->user_model->get_user_name($this->session->id)[0]->nimi,
                    'nimi' => $recipe['title'],
                    'ajakulu' => $recipe['prep'],
                    'mitmele' => $recipe['servings'],
                    'kirjeldus' => $recipe['description'],
                    'juhend' => $recipe['directions'],
                    'sildid' => implode(', ', $recipe['tags']),
                    'koostisosad' => $this->ingredient_model->get_ingredients($id),
                    'pilt' => $image_status
                ));
            }
            else redirect(base_url('retsept/lisa', 'https:'));
        } else{
            $koostisosad = array();
            $i=0;
            while ($i<intval(set_value('aineid'))){
                if (set_value('aine'.$i) == "" || set_value('ühik'.$i) == "" || set_value('kogus'.$i) == "") {
                    $i+=1;
                    continue;
                }
                $koostisosa = new stdClass();
                $koostisosa->name = html_escape(set_value('aine'.$i));
                $koostisosa->unit = html_escape(set_value('ühik'.$i));
                $koostisosa->amount = html_escape(set_value('kogus'.$i));
                array_push($koostisosad, $koostisosa);
                $i+=1;
            };
            $this->load->view('retsept_lisa', 
                array(
                    'id'=> $id,
                    'autor'=> $this->user_model->get_user_name($this->session->id)[0]->nimi,
                    'nimi' => set_value('pealkiri'),
                    'ajakulu' => set_value('ajakulu'),
                    'mitmele' => set_value('mitmele'),
                    'kirjeldus' => set_value('kirjeldus'),
                    'juhend' => set_value('juhend'),
                    'sildid' => set_value('sildid'),
                    'koostisosad' => $koostisosad,
                    'pilt' => $image_status
                ));
        }
        $this->load->view('footer');   
   }

    # Executed without JavaScript
    public function sync_otsing(){

        $this->load->model('user_model');
        $this->user_model->auth();

        # Search for recipes based on combination of parameters
        $this->load->model('recipe_model');

        # Gather all parameters to be taken into account
        $views = array();
        if ($this->input->get('tags'))  {
            $tags = $this->input->get('tags');
            $tags = preg_split("/\\s*,\\s*/", trim($tags));
            array_push($views, $this->recipe_model->get_tags_query($tags));
        }
        if ($this->input->get('keywords'))  {
            array_push($views, $this->recipe_model->get_keywords_query($this->input->get('keywords')));
        }

        #no search parameters or no results
        if(sizeof($views) == 0){
            $this->load->view('navigatsioon',array('title' => "Retsepti otsing"));
            $this->load->view('retsept_otsing');
            return;
        }

        # Displays search results in a new view
        $recipes = $this->recipe_model->get_results($views);
        $data['recipes']=$recipes;
        $this->load->view('navigatsioon',array('title' => 'Otsingu tulemused'));
        $this->load->view('retsept_otsing');
        $this->load->view('otsingu_tulemused', $data);
        $this->load->view('footer');
    }

    # Composes XML file with search results for XMLHttpRequest response
    public function otsing() {
        $this->load->model('user_model');
        $this->user_model->auth();
	    # Search for recipes based on combination of parameters
        # Result as XML
        header("Content-Type:text/xml");
        $this->load->helper('xml');
        $this->load->model('recipe_model');

        # Gather all parameters to be taken into account
	    $views = array();
        if ($this->input->get('tags'))  {
            $tags = $this->input->get('tags');
            $tags = preg_split("/\\s*,\\s*/", trim($tags));
            array_push($views, $this->recipe_model->get_tags_query($tags));
        }
        if ($this->input->get('keywords'))  {
            array_push($views, $this->recipe_model->get_keywords_query($this->input->get('keywords')));
        }

        # Database search
        $recipes = $this->recipe_model->get_results($views);

        $domtree = new DOMDocument('1.0', 'UTF-8');

        $xmlRoot = $domtree->createElement("tulemused");
        $xmlRoot = $domtree->appendChild($xmlRoot);
        foreach ($recipes as $recipe) {
            $xmlrecipe = $domtree->createElement("retsept");
            $xmlrecipe->setAttribute("id", xml_convert($recipe['id']));
            $xmlrecipe = $xmlRoot->appendChild($xmlrecipe);
            $xmlrecipe->appendChild($domtree->createElement('nimi', xml_convert($recipe['title'])));
            $xmlrecipe->appendChild($domtree->createElement('kirjeldus', xml_convert($recipe['description'])));
        }
        echo $domtree->saveXML();

    }



}
