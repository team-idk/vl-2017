<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konto extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        require_once 'push/vendor/sentry/sentry/lib/Raven/Autoloader.php';
        Raven_Autoloader::register();
        $client = new Raven_Client('https://45dceb073fe74671b17057201bec938c:fbb98949242641949a2985a52ee08d14@sentry.io/151789');
        $client->install();
    }

    public function index()
    {
        $this->load->model('user_model');
        $this->load->model('recipe_model');

        $this->user_model->auth();
        header("Content-Type: application/xhtml+xml; charset=utf-8");
        $this->load->view('navigatsioon', array('title' => 'Kasutaja konto'));
        $id = $this->session->id;
        $this->load->view('konto',array(
                'nimi' => $this->user_model->get_user_name($id)[0]->nimi,
                'isikukood' => $id,
                'recipes' => (array) $this->recipe_model->userrecipes($id)
            )
        );
        $this->load->view('footer');


    }

}