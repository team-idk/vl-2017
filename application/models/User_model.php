<?php

class User_model extends CI_Model
{
    public function auth($required = false)
    {
        if (isset($_SERVER['HTTPS'])) {
            if (array_key_exists('SSL_CLIENT_S_DN', $_SERVER) && $_SERVER['SSL_CLIENT_VERIFY'] == 'SUCCESS') {
                $authvalues = preg_split('/(?<!\\\\),/', $_SERVER['SSL_CLIENT_S_DN']);
                $auth = array();
                foreach ($authvalues as $authvalue) {
                    $authvalue = explode('=', $authvalue);
                    if (sizeof($authvalue) != 2) continue;
                    $auth[$authvalue[0]] = $authvalue[1];
                }
                if (array_key_exists('C', $auth) && array_key_exists('O', $auth) && array_key_exists('OU', $auth) &&
                    $auth['C'] == 'EE' && $auth['O'] == 'ESTEID' && $auth['OU'] == 'authentication'
                ) {
                    $this->session->fname = $auth['GN'];
                    $this->session->lname = $auth['SN'];
                    $this->session->id = $auth['serialNumber'];
                    $this->update_user();
                    $this->update_session();
                }
            } else {
                $this->check_login();
                if (!$this->session->has_userdata('id')) {
                    $this->load->helper('url');
                    if (!isset($this->session->pre_login_url)) {
                        $this->session->pre_login_url = uri_string();
                    }
                    redirect('/user/login');
                }
            }
        }
    }

    public function update_user()
    {
        session_write_close();
        $sql = "CALL update_user(?, ?, ?)";
        $this->db->query($sql, array(
            $this->session->id,
            $this->session->fname,
            $this->session->lname
            ));
    }

    public function check_login()
    {
        $sql = "SELECT idcode FROM sessions_view WHERE sessid = ?";
        $result = $this->db->query($sql, $this->session->session_id)->result_array();
        if (sizeof($result) > 0) {
            $id = $result[0]['idcode'];
            $sql = "SELECT * FROM users_view WHERE id = ?";
            $result = $this->db->query($sql, $id)->result_array();
            $this->session->fname = $result[0]['fname'];
            $this->session->lname = $result[0]['lname'];
            $this->session->id = $result[0]['id'];
        }
    }

    public function deauth() {
        $sql = "CALL destroy_session(?)";
        $this->db->query($sql, array(
            $this->session->session_id,
        ));
        $this->session->sess_destroy();
    }

    public function update_session()
    {
        $sql = "CALL update_session(?, ?)";
        $this->db->query($sql, array(
            $this->session->session_id,
            $this->session->id,
        ));
    }
    
    public function get_user_name($id)
    {
        $sql = "SELECT CONCAT(fname, ' ', lname) AS nimi FROM users_view WHERE id = ?";
        return (array)$this->db->query($sql, array($id))->result();
    }
}

?>