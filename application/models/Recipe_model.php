<?php

class Recipe_model extends CI_Model
{

    public function get_recipe($id)
    {
        $sql = "SELECT * FROM recipes_view WHERE id = ?";
        $recipe = (array)$this->db->query($sql, array($id))->result_array()[0];
        $recipe['tags'] = array();
        $sql = "SELECT text AS tag FROM recipes_view
  JOIN taggings_view ON recipes_view.id = taggings_view.recipe
  JOIN tags_view ON taggings_view.tag = tags_view.id
WHERE recipes_view.id = ?;";
        $tags = $this->db->query($sql, array($id))->result_array();
        foreach ($tags as $tag) {
            array_push($recipe['tags'], $tag['tag']);
        }
        return $recipe;
    }

    public function get_recipename($id)
    {
        $sql = "SELECT * FROM recipes_view WHERE id = ?";
        $recipe = (array)$this->db->query($sql, array($id))->result_array()[0];
        return $recipe['title'];
    }

    public function get_results($views)
    {
        # Execute query that selects intersection of all provided queries
        $sqls = array();
        $args = array();
        for ($i = 0; $i < sizeof($views); $i++) {
            array_push($sqls, '(' . $views[$i][0] . ') AS T' . $i);
            $args = array_merge($args, $views[$i][1]);
        }
        $sql = 'SELECT * FROM ' . implode(' JOIN ', $sqls);
        if (sizeof($sqls) > 1) {
            $sql .= ' USING (id)';
        }
        return $this->db->query($sql . ';', $args)->result_array();
    }

    public function get_keywords_query($keywords)
    {
        # Returns sub-query to select recipes based on keyword search
        $sql = 'SELECT * FROM recipes_view ' .
            'WHERE MATCH(title, description) AGAINST (? IN NATURAL LANGUAGE MODE WITH QUERY EXPANSION)';
        return array($sql, array($keywords));
    }

    public function get_tags_query($tags)
    {
        # Returns sub-query to select recipes based on tags
        $sql = 'SELECT recipes_view.* FROM recipes_view JOIN taggings_view JOIN tags_view ' .
            'ON recipes_view.id = taggings_view.recipe AND taggings_view.tag = tags_view.id ' .
            'WHERE tags_view.text IN ? ' .
            'GROUP BY recipes_view.id ' .
            'HAVING count(*) = ?';
        $bindings = array($tags, sizeof($tags));
        return array($sql, $bindings);

    }


    public function ingredient_exists($koostisosa){
        $sql = "SELECT * FROM ingredients_view WHERE name = ? and unit = ?";
        $ingredients = (array)$this->db->query($sql, array($koostisosa->name, $koostisosa->unit))->result_array();
        return sizeof($ingredients) > 0;
    }

    public function inclusion_exists($koostisosaid, $id){
        $sql = "SELECT * FROM inclusions_view WHERE recipe = ? and ingredient = ?";
        $inclusions = (array)$this->db->query($sql, array($id, $koostisosaid))->result_array();
        return sizeof($inclusions) > 0;


    }

    public function get_ingredient($koostisosa){
        $sql = "SELECT * FROM ingredients_view WHERE name = ? and unit = ?";
        return (array)$this->db->query($sql, array($koostisosa->name, $koostisosa->unit))->result_array()[0]['id'];


    }

    public function store($recipe) {
        if ($recipe['id'] == null) {
            $id = $this->db->query('SELECT store_recipe(?, ?, ?, ?, ?, ?) AS id;',
                array($recipe['pealkiri'], $recipe['kirjeldus'], $recipe['juhend'], $recipe['ajakulu'], $recipe['mitmele'], $this->session->id))->result_array()[0]['id'];
        } else {
            $this->db->query('CALL update_recipe(?, ?, ?, ?, ?, ?)',
            array($recipe['id'], $recipe['pealkiri'], $recipe['kirjeldus'], $recipe['juhend'], $recipe['ajakulu'], $recipe['mitmele']));
            $id = $recipe['id'];
        }
        $tags = preg_split ('@\s*,\s*@', $recipe['sildid']);
        foreach ($tags as $tag) {
            if ($tag == "") continue;
            $this->db->query('CALL tag(?, ?)',
                array($id, $tag));
        }
        $this->db->query('CALL clean_inclusion(?)', array($id));
        $koostisosad = $recipe['koostisosad'];
        foreach ($koostisosad as $koostisosa) {
            if ($this->ingredient_exists($koostisosa)){
                $koostisosaid = $this->get_ingredient($koostisosa);
                if ($this->inclusion_exists($koostisosaid, $id)){
                    $inclusionid = $this->db->query('CALL update_inclusion(?,?,?)', array($id, $koostisosa->amount, $koostisosaid));
                } else {
                    $inclusionid = $this->db->query('SELECT store_inclusion(?,?,?) AS id;', array($id, $koostisosa->amount, $koostisosaid))->result_array()[0]['id'];
                }
            }
            else {
                $koostisosaid = $this->db->query('SELECT store_ingredient(?,?) AS id;', array($koostisosa->name, $koostisosa->unit))->result_array()[0]['id'];
                $inclusionid=$this->db->query('SELECT store_inclusion(?,?,?) AS id;', array($id, $koostisosa->amount, $koostisosaid))->result_array()[0]['id'];
            }

        }
        return $id;
    }


    public function get_all_recipes(){
        $sql = "SELECT * FROM recipes_view";
        $recipe = (array)$this->db->query($sql, array())->result_array();
        return $recipe;
    }
    
    public function remove_recipe($id){
        $this->db->query('CALL remove_recipe(?)', array($id));
        
    }

    public function userrecipes($userid){
        return (array)$this->db->query("SELECT * FROM recipes_view where author=? ",array($userid))->result_array();
    }
}

?>