<?php


class Shoppinglist_model extends CI_Model
{
    public function get_shoppinglist($user){
        $this->load->model('recipe_model');

        $sql = "SELECT * FROM shoppinglist_view WHERE user = ?";
        $groceries = (array)$this->db->query($sql, array($user))-> result();
        foreach ($groceries as $g){
            $g->recipename = $this->recipe_model->get_recipename($g->recipe);

        }
        return $groceries;
    }

    public function update_shoppinglist($list){
        foreach ($list['ingredients'] as $ingredient) {
            $this->db->query('CALL update_list(?, ?,?,?)',
                array($ingredient->unit, $ingredient->amount, $ingredient->name, $ingredient->id));
        }
    }

    public function removefromlist($id){
        $this->db->query('CALL remove_list(?)', array($id));
    }

    public function store_recipe_to_shoppinglist($list)
    {
        foreach ($list['ingredients'] as $ingredient) {
            $id = $this->db->query('SELECT store_list(?, ?, ?,?,?) AS id;',
                array($list['recipe'], $ingredient->amount, $ingredient->name,$ingredient->unit, $list['user']))->result_array()[0]['id'];

        }
    }

    public function inshoppinglist($userid, $recipe){
        $sql = "SELECT * FROM shoppinglist_view WHERE user = ? and recipe = ?";
        $ingredients = (array)$this->db->query($sql, array($userid, $recipe))->result_array();
        return sizeof($ingredients) > 0;
    }
}