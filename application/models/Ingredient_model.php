<?php

class Ingredient_model extends CI_Model
{

    public function get_ingredients($recipe)
    {
        $sql = "SELECT * FROM ingredients_view JOIN inclusions_view " .
          "ON ingredients_view.id = inclusions_view.ingredient WHERE recipe = ?";
        return (array)$this->db->query($sql, array($recipe))->result();
    }
}

?>