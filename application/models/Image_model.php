<?php

class Image_model extends CI_Model
{
    public function store_image($recipe,$image,$mime){
        return $this->db->query('SELECT store_image(?, ?, ?) AS id;',
            array($recipe, $image, $mime))->result_array()[0]['id'];

    }

    public function remove_image($imageid){
        $this->db->query('CALL remove_image(?)', array($imageid));

    }

    public function update_image($imageid, $image, $mime){
        $this->db->query('CALL update_image(?, ?, ?)',
            array($imageid,$image, $mime));

    }

    public function get_image($recipe) {
        $sql = "SELECT image, mime FROM images_view WHERE recipe = ? ORDER BY id DESC LIMIT 1";
        return (array)$this->db->query($sql, array($recipe))->result_array();
    }

}