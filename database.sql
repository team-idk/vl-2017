CREATE DATABASE vl_2017;
# CREATE USER 'codeigniter'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE ON vl_2017.* to 'codeigniter'@'localhost';
USE vl_2017;

CREATE TABLE users (
  id CHAR(11) PRIMARY KEY,
  fname VARCHAR(255),
  lname VARCHAR(255)
);

CREATE TABLE recipes (
  id          INT AUTO_INCREMENT PRIMARY KEY,
  title       VARCHAR(255),
  description TEXT,
  directions  TEXT,
  servings    INT,
  prep        TIME,
  author      CHAR(11),
  FOREIGN KEY (author) REFERENCES users (id) ON DELETE CASCADE,
  FULLTEXT (title, description)
);

CREATE TABLE ingredients (
  id   INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255),
  unit VARCHAR(4)
);

CREATE TABLE shoppinglist (
  id   INT AUTO_INCREMENT PRIMARY KEY,
  amount    INT,
  recipe INT,
  user  CHAR(11),
  name VARCHAR(255),
  unit VARCHAR(4),
  FOREIGN KEY (recipe) REFERENCES recipes (id) ON DELETE CASCADE,
  FOREIGN KEY (user) REFERENCES users (id) ON DELETE CASCADE

);

CREATE VIEW shoppinglist_view AS SELECT * FROM shoppinglist;



DELIMITER //
CREATE PROCEDURE remove_list(shoppinglist_i INT)
  BEGIN
    DELETE FROM shoppinglist WHERE id = shoppinglist_i;
  END//

DELIMITER //
CREATE FUNCTION store_list(recipe_i INT,amount_i INT,name_i VARCHAR(255),unit_i VARCHAR(4), user_i CHAR(11))
  RETURNS INT
NOT DETERMINISTIC
  BEGIN
    INSERT INTO shoppinglist(recipe,unit,user,amount,name)
    VALUES(recipe_i,unit_i,user_i,amount_i,name_i);
    RETURN LAST_INSERT_ID();
  END//
DELIMITER //

DELIMITER //
CREATE PROCEDURE update_list(unit_i VARCHAR(4), amount_i INT, name_i VARCHAR(255), list_i INT)
  BEGIN
    UPDATE shoppinglist SET
      amount = amount_i,
      unit = unit_i,
      name = name_i
    WHERE id=list_i;
  END //

CREATE TABLE inclusions (
  recipe     INT,
  ingredient INT,
  amount     INT,
  FOREIGN KEY (recipe) REFERENCES recipes (id) ON DELETE CASCADE,
  FOREIGN KEY (ingredient) REFERENCES ingredients (id) ON DELETE CASCADE,
  PRIMARY KEY (recipe, ingredient)
);

CREATE TABLE tags (
  id   INT AUTO_INCREMENT PRIMARY KEY,
  text VARCHAR(255),
  UNIQUE (text)
);

CREATE TABLE taggings (
  recipe INT,
  tag    INT,
  FOREIGN KEY (recipe) REFERENCES recipes (id) ON DELETE CASCADE,
  FOREIGN KEY (tag) REFERENCES tags (id) ON DELETE CASCADE,
  PRIMARY KEY (recipe, tag)
);

CREATE TABLE sessions (
  sessid VARCHAR(128) PRIMARY KEY,
  idcode CHAR(11),
  FOREIGN KEY (idcode) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE images (
  id   INT AUTO_INCREMENT PRIMARY KEY,
  image BLOB,
  recipe INT,
  FOREIGN KEY (recipe) REFERENCES recipes (id) ON DELETE CASCADE
);

CREATE VIEW images_view AS SELECT * FROM images;

DELIMITER //
CREATE FUNCTION store_image(recipe_i INT, image_i BLOB, mime_i VARCHAR(255))
  RETURNS INT
NOT DETERMINISTIC
  BEGIN
    INSERT INTO images(recipe, image, mime)
    VALUES(recipe_i, image_i, mime_i);
    RETURN LAST_INSERT_ID();
  END//

DELIMITER //
CREATE PROCEDURE remove_image(image_i INT)
  BEGIN
    DELETE FROM images WHERE id = image_i;
  END//

DELIMITER //
CREATE PROCEDURE update_image(imageid_i INT, image_i BLOB)
  BEGIN
    UPDATE images SET
      image = image_i
    WHERE id=imageid_i;
  END //

CREATE VIEW users_view AS SELECT * FROM users;
CREATE VIEW recipes_view AS SELECT * FROM recipes;
CREATE VIEW ingredients_view AS SELECT * FROM ingredients;
CREATE VIEW inclusions_view AS SELECT * FROM inclusions;
CREATE VIEW tags_view AS SELECT * FROM tags;
CREATE VIEW taggings_view AS SELECT * FROM taggings;
CREATE VIEW sessions_view AS SELECT * FROM sessions;

DELIMITER //
CREATE PROCEDURE update_user(id_i CHAR(11), fname_i VARCHAR(255), lname_i VARCHAR(255))
  BEGIN
    INSERT INTO users (id, fname, lname) VALUES(id_i, fname_i, lname_i)
    ON DUPLICATE KEY UPDATE fname=fname_i, lname=lname_i;
  END//

DELIMITER //
CREATE PROCEDURE update_session(sessid_i VARCHAR(128), idcode_i CHAR(11))
  BEGIN
    INSERT INTO sessions (sessid, idcode) VALUES(sessid_i, idcode_i)
    ON DUPLICATE KEY UPDATE idcode=idcode_i;
  END//

DELIMITER //
CREATE PROCEDURE destroy_session(sessid_i VARCHAR(128))
  BEGIN
    DELETE FROM sessions WHERE sessid=sessid_i;
  END//

DELIMITER //
CREATE FUNCTION store_recipe(title_i VARCHAR(255),
                              description_i TEXT,
                              directions_i TEXT,
                              prep_i TIME,
                              servings_i INT,
                              author_i CHAR(11))
  RETURNS INT
  NOT DETERMINISTIC
  BEGIN
    INSERT INTO recipes (title, description, directions, prep, servings, author)
    VALUES(title_i, description_i, directions_i, prep_i, servings_i, author_i);
    RETURN LAST_INSERT_ID();
  END//

DELIMITER //
CREATE FUNCTION store_inclusion(recipe_i INT, amount_i INT, ingredient_i INT)
  RETURNS INT
NOT DETERMINISTIC
  BEGIN
    INSERT INTO inclusions(recipe, amount, ingredient)
    VALUES(recipe_i, amount_i, ingredient_i);
    RETURN LAST_INSERT_ID();
  END//

DELIMITER //
CREATE PROCEDURE clean_inclusion(recipe_i INT)
  BEGIN
    DELETE FROM inclusions
    WHERE recipe=recipe_i;
  END //

DELIMITER //
CREATE PROCEDURE update_inclusion(recipe_i INT, amount_i INT, ingredient_i INT)
  BEGIN
    UPDATE inclusions SET
      amount = amount_i
    WHERE recipe=recipe_i and ingredient=ingredient_i;
  END //

DELIMITER //
CREATE FUNCTION store_ingredient(name_i VARCHAR(255), unit_i VARCHAR(4))
  RETURNS INT
NOT DETERMINISTIC
  BEGIN
    INSERT INTO ingredients(name,unit)
    VALUES(name_i,unit_i);
    RETURN LAST_INSERT_ID();
  END//



CREATE PROCEDURE update_recipe(id_i INT,
                               title_i VARCHAR(255),
                               description_i TEXT,
                               directions_i TEXT,
                               prep_i TIME,
                               servings_i INT)
  BEGIN
    UPDATE recipes SET
      title=title_i,
      description=description_i,
      directions=directions_i,
      prep=prep_i,
      servings=servings_i
    WHERE id = id_i;
  END //


DELIMITER //
CREATE PROCEDURE tag(recipe_i INT, text_i VARCHAR(255))
  BEGIN
    INSERT IGNORE INTO tags (text) VALUES(text_i);
    INSERT IGNORE INTO taggings (recipe, tag) VALUES (recipe_i, (SELECT id FROM tags WHERE text=text_i));
  END//
  
DELIMITER //
CREATE PROCEDURE remove_recipe(recipe_i INT)
  BEGIN
    DELETE FROM inclusions WHERE recipe = recipe_i;
    DELETE FROM taggings WHERE recipe = recipe_i;
    DELETE FROM recipes WHERE id = recipe_i;
  END//