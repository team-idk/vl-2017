<?php

$lang['profiler_benchmarks']	= 'J&Otilde;UDLUSTEST';
$lang['profiler_queries']		= 'P&Auml;RING';
$lang['profiler_get_data']		= 'GET DATA';
$lang['profiler_post_data']		= 'POST DATA';
$lang['profiler_uri_string']	= 'URI STRING';
$lang['profiler_memory_usage']	= 'M&Auml;LU KASUTUS';
$lang['profiler_no_db']			= 'Andmebaasi ajam pole praegu laetud';
$lang['profiler_no_queries']	= 'J&auml;relpärimisi pole sooritatud';
$lang['profiler_no_post']		= 'POST andmed puuduvad';
$lang['profiler_no_get']		= 'GET andmed puuduvad';
$lang['profiler_no_uri']		= 'URI andmed puuduvad';
$lang['profiler_no_memory']		= 'M&auml;lukasutus pole saadaval';

/* End of file profiler_lang.php */
/* Location: ./system/language/estonian/profiler_lang.php */