<?php

$lang['scaff_view_records']		= 'Vaata kirjeid';
$lang['scaff_create_record']	= 'Loo uued kirjed';
$lang['scaff_add']				= 'Lisa andmed';
$lang['scaff_view']				= 'Vaata andmeid';
$lang['scaff_edit']				= 'Muuda';
$lang['scaff_delete']			= 'Kustuta';
$lang['scaff_view_all']			= 'Vaata k&otilde;iki';
$lang['scaff_yes']				= 'Jah';
$lang['scaff_no']				= 'Ei';
$lang['scaff_no_data']			= 'Selle tabelijaoks andmeid veel pole.';
$lang['scaff_del_confirm']		= 'Oled sa kindel, et soovid kustutada j&auml;rgneva rea:';


/* End of file scaffolding_lang.php */
/* Location: ./system/language/estonian/scaffolding_lang.php */