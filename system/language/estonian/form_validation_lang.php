<?php

$lang['required'] 			= " %s väli on nõutud.";
$lang['isset']				= " %s väli peab omama väärtusi.";
$lang['valid_email']		= " %s väli peab sisaldama kehtivat e-kirja aadressi.";
$lang['valid_emails'] 		= " %s väli peab sisaldama kehtivaid e-kirja aadresse.";
$lang['valid_url'] 			= " %s väli peab sisaldama kehtivat URL aadressi.";
$lang['valid_ip'] 			= " %s väli peab sisaldama a valid IP.";
$lang['min_length']			= " %s väli peab olema vähemalt %s tähemärki pikk.";
$lang['max_length']			= " %s väli ei tohi &uuml;letada pikkuselt %s tähemärki.";
$lang['exact_length']		= " %s väli peab olema pikkuselt täpselt %s tähemärki.";
$lang['alpha']				= " %s väli võib sisaldada ainult tähestiku tähti.";
$lang['alpha_numeric']		= " %s väli võib sisaldada ainult tähestiku tähti ja numbreid.";
$lang['alpha_dash']			= " %s väli võib sisaldada ainult tähestiku tähti, numbreid, allkriipsu ja kaldkriipsu.";
$lang['numeric']			= " %s väli peab sisaldama ainult numbreid.";
$lang['is_numeric']			= " %s väli peab sisaldama ainult numbrilisi märke.";
$lang['integer']			= " %s väli peab sisaldama täisarve.";
$lang['matches']			= " %s väli ei vasta %s väljale.";
$lang['regex_match']		= " %s väli ei vasta nõutud formaadile.";
$lang['is_natural']			= " %s väli peab sisaldama ainut positiivseid numbreid.";
$lang['is_natural_no_zero']	= " %s väli peab sisaldama nullist suuremat väärtust.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/estonian/form_validation_lang.php */