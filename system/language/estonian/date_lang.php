<?php

$lang['date_year'] = "Aasta";
$lang['date_years'] = "Aastat";
$lang['date_month'] = "Kuu";
$lang['date_months'] = "Kuud";
$lang['date_week'] = "N&auml;dal";
$lang['date_weeks'] = "N&auml;dalat";
$lang['date_day'] = "P&auml;ev";
$lang['date_days'] = "P&auml;eva";
$lang['date_hour'] = "Tund";
$lang['date_hours'] = "Tundi";
$lang['date_minute'] = "Minut";
$lang['date_minutes'] = "Minutit";
$lang['date_second'] = "Sekund";
$lang['date_seconds'] = "Sekundit";

$lang['UM12']	= "(UTC - 12:00) Eniwetok, Kwajalein";
$lang['UM11']	= "(UTC - 11:00) Nome, Midway Island, Samoa";
$lang['UM10']	= "(UTC - 10:00) Hawaii";
$lang['UM9']	= "(UTC - 9:00) Alaska";
$lang['UM8']	= "(UTC - 8:00) Pacific Time";
$lang['UM7']	= "(UTC - 7:00) Mountain Time";
$lang['UM6']	= "(UTC - 6:00) Central Time, Mexico City";
$lang['UM5']	= "(UTC - 5:00) Eastern Time, Bogota, Lima, Quito";
$lang['UM4']	= "(UTC - 4:00) Atlantic Time, Caracas, La Paz";
$lang['UM25']	= "(UTC - 3:30) Newfoundland";
$lang['UM3']	= "(UTC - 3:00) Brazil, Buenos Aires, Georgetown, Falkland Is.";
$lang['UM2']	= "(UTC - 2:00) Mid-Atlantic, Ascension Is., St. Helena";
$lang['UM1']	= "(UTC - 1:00) Azores, Cape Verde Islands";
$lang['UTC']	= "(UTC) Casablanca, Dublin, Edinburgh, London, Lisbon, Monrovia";
$lang['UP1']	= "(UTC + 1:00) Berlin, Brussels, Copenhagen, Madrid, Paris, Rome";
$lang['UP2']	= "(UTC + 2:00) Kaliningrad, South Africa, Warsaw, Tallinn";
$lang['UP3']	= "(UTC + 3:00) Baghdad, Riyadh, Moscow, Nairobi";
$lang['UP25']	= "(UTC + 3:30) Tehran";
$lang['UP4']	= "(UTC + 4:00) Abu Dhabi, Baku, Muscat, Tbilisi";
$lang['UP35']	= "(UTC + 4:30) Kabul";
$lang['UP5']	= "(UTC + 5:00) Islamabad, Karachi, Tashkent";
$lang['UP45']	= "(UTC + 5:30) Mumbai, Kolkata, Chennai, New Delhi";
$lang['UP6']	= "(UTC + 6:00) Almaty, Colomba, Dhaka";
$lang['UP7']	= "(UTC + 7:00) Bangkok, Hanoi, Jakarta";
$lang['UP8']	= "(UTC + 8:00) Beijing, Hong Kong, Perth, Singapore, Taipei";
$lang['UP9']	= "(UTC + 9:00) Osaka, Sapporo, Seoul, Tokyo, Yakutsk";
$lang['UP85']	= "(UTC + 9:30) Adelaide, Darwin";
$lang['UP10']	= "(UTC + 10:00) Melbourne, Papua New Guinea, Sydney, Vladivostok";
$lang['UP11']	= "(UTC + 11:00) Magadan, New Caledonia, Solomon Islands";
$lang['UP12']	= "(UTC + 12:00) Auckland, Wellington, Fiji, Marshall Islands";


/* End of file date_lang.php */
/* Location: ./system/language/estonian/date_lang.php */