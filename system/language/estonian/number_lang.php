<?php

$lang['terabyte_abbr'] = "TB";
$lang['gigabyte_abbr'] = "GB";
$lang['megabyte_abbr'] = "MB";
$lang['kilobyte_abbr'] = "KB";
$lang['bytes'] = "Baite";

/* End of file number_lang.php */
/* Location: ./system/language/estonian/number_lang.php */