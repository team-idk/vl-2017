<?php

$lang['upload_userfile_not_set'] = "Ei leia muutujat nimega kasutaja fail.";
$lang['upload_file_exceeds_limit'] = "Üleslaetava faili suurus on enam kui lubatud PHP seadistustes.";
$lang['upload_file_exceeds_form_limit'] = "Üleslaetava faili suurus ületab laadimisvormis lubatud suurust.";
$lang['upload_file_partial'] = "Fail sai üleslaetud poolikult.";
$lang['upload_no_temp_directory'] = "Ajutine kaust puudub.";
$lang['upload_unable_to_write_file'] = "Faili ei saanud kettale kirjutada.";
$lang['upload_stopped_by_extension'] = "Üleslaadimine peatatati laiendi poolt.";
$lang['upload_no_file_selected'] = "Sa ei valinud faili mida üles laadida.";
$lang['upload_invalid_filetype'] = "Faili tüüp mida sa püüad üles laadida ei ole lubatud.";
$lang['upload_invalid_filesize'] = "Faili mida sa püüad üles laadida on lubatust suurem.";
$lang['upload_invalid_dimensions'] = "Pilt mida sa proovid üles ladida on suurem lubatust kõrguselt/laiuselt.";
$lang['upload_destination_error'] = "Ilmens viga, püüdes teisaldada üleslaetud faili tema lõpplikule kohale.";
$lang['upload_no_filepath'] = "Üleslaadimise adress ei tundu õige olevat.";
$lang['upload_no_file_types'] = "Sa ei ole m&auml;&auml;ranud ühtegi lubatud faili tüüpi.";
$lang['upload_bad_filename'] = "Sinu pakutud nimega fail on juba serveris olemas.";
$lang['upload_not_writable'] = "Üleslaadimise kausta ei saa kirjutada.";


/* End of file upload_lang.php */
/* Location: ./system/language/estonian/upload_lang.php */