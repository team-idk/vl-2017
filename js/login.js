const conn = new WebSocket('wss://retseptikum.ervinoro.eu:8080');
conn.onopen = function (e) {
    console.log("Connection established!");
};
conn.onmessage = function (e) {
    console.log(e.data);
    if (e.data === 'You have been logged in!') {
        window.location = '/user/redirect';
    } else if (e.data === 'No other clients') {
        $('#loginModal').modal('hide');
        alert("Näib, et te pole mujal sisse logitud. Palun logige sisse mõnest muust seadmest ning hoidke sealne aken avatud.")
    } else if (confirm("Kas soovite lubada sisselogimise?")) {
        conn.send(e.data);
    }
};
function sendLoginRequest() {
    const idcode = document.getElementById("idcode").value;
    if (conn.readyState === WebSocket.OPEN) {
        conn.send(idcode);
        $('#loginModal').modal();
    } else {
        conn.onopen = function (e) {
            console.log("Connection established!");
            conn.send(idcode);
            $('#loginModal').modal();
        };
    }
    return false;
}
function backDoor() {
    if (conn.readyState === WebSocket.OPEN) {
        conn.send("backdoor");
    } else {
        conn.onopen = function (e) {
            console.log("Connection established!");
            conn.send("backdoor");
        };
    }
    return false;
}
