//delay image loading
$(window).on('load',(function() {
    $('div[data-image-src]').prepend(function(){
        let src = $(this).attr('data-image-src');
        return '<img alt="Retsepti illustratsioon" src="'+ src +'" class="img-large"/>';
    })
}));