const recipes = [];

window.onpopstate = function (event) {
    let currentState = history.state;
    if(currentState != null) {
        document.body.innerHTML = currentState.innerhtml;
    } else {
        const results = document.getElementsByClassName("search_result");
        while (results.length > 0) {
            results[0].parentNode.removeChild(results[0]);
        }
    }
};


function sendXHR(formElement) {

	//create XMLHttpRequest object
	let url = "//retseptikum.ervinoro.eu/index.php/retsept/otsing";
	let xhr;
	if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	} else {
		// for IE6, IE5
		xhr = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xhr.responseType='document';

	//create query from submitted form element
	const inputs = formElement.getElementsByTagName("input");
	let fullquery = "?";
	let valueNull = true;
	for (index = 0; index < inputs.length; ++index) {
		if (inputs[index]["type"] != "submit") {
			let element = inputs[index];
			let name = encodeURI(element["name"]);
			let value = encodeURI(element["value"]);
			if (value.length>0){
				valueNull = false;
			}
			fullquery += name + "=" + value;
			if (index < inputs.length - 2) {
				fullquery += "&"
			}
		}

	}
	url += fullquery;
    xhr.open(formElement.method, url, true);

    //nothing to request, form was empty
    if (valueNull) {
		return false;
	}
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	xhr.send("");
	xhr.onload = function () {
		parseXML.call(this, fullquery);

	};
	return false;
}

//creates document elements from responseXML
function parseXML(fullquery) {

    //remove previous search results displayed in the document
    recipes.length = 0;
    const results = document.getElementsByClassName("search_result");
    while (results.length > 0) {
        results[0].parentNode.removeChild(results[0]);
    }
    const footerbuffer = document.getElementById("footer-buffer");
    if (footerbuffer !== null){
        footerbuffer.parentNode.removeChild(footerbuffer);
    }

    //create new element for search results
    let div = document.createElement('div');
    div.className = "container-fluid";
    div.id = "result-list";

    let ul = document.createElement('ul');
    ul.className = "search_result";

    //use XPath to find data from response
    const doc = this.responseXML;
    let retseptidPath;
    if (doc.evaluate) {
        retseptidPath = doc.evaluate('//retsept[@id and nimi]', doc, null, XPathResult.ANY_TYPE, null);
    } else {
        retseptidPath = doc.evaluate('//retsept[@id and nimi]'); //for IE

    }

    //transform all data to Recipe objects for better maintenance
    let data = retseptidPath.iterateNext();
    while (data) {
        recipes.push(new Recipe(data.attributes.getNamedItem('id').textContent,data.firstChild.textContent));
        data = retseptidPath.iterateNext();
    }

    //fill elements with found data
    let index = 0;
    for (index = 0; index < recipes.length; ++index) {
        let name = recipes[index].name;
        let tname = document.createTextNode(name);

        let id = recipes[index].id;
        let href = "//retseptikum.ervinoro.eu/index.php/retsept/detailid/"+id;

        let li = document.createElement("li");
        let a = document.createElement("a");
        a.setAttribute("href",href);
        a.appendChild(tname);
        li.appendChild(a);
        ul.appendChild(li);

    }
    div.appendChild(ul);



    //button for loafing more results
    if (recipes.length > 0) {

        const btn = document.createElement("button");
        btn.className = "search_result";
        btn.id = "show_more_btn";
        let t = document.createTextNode("Rohkem tulemusi");
        btn.appendChild(t);
        div.appendChild(btn);
    }
    else {
        const p = document.createElement("p");
        p.className = "search_result";
        let t = document.createTextNode("Ei leidnud tulemusi");
        p.appendChild(t);
        div.appendChild(p);

    }
    document.body.appendChild(div);
    const pbuffer = document.createElement("p");
    pbuffer.id = "footer-buffer";
    pbuffer.appendChild(document.createElement("br"));
    pbuffer.appendChild(document.createElement("br"));
    document.body.appendChild(pbuffer);


    window.history.pushState({ url: window.location.href, innerhtml: document.body.innerHTML },document.title,"/index.php/retsept/sync_otsing"+fullquery);

}


class Recipe {
	constructor( id, name ) {
		this.name = name;
		this.id = id;

	}

}


