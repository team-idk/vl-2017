$(function(){
    // Enables popover
    $("#shopping").popover({
        html : true, 
        content: function() {
          return $("#shopping-list").html();
        }
    });
});

function hideShopping() {
    //hides shopping list popover
    $('#shopping').popover('hide');
    $('#shopping').data("bs.popover").inState.click = false;
}

function addIngredient() {
    //adds a row to form for new ingredient
    var table = table = document.getElementById("ingredients");
    var row = table.insertRow(-1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var id = parseInt(document.getElementById("aineid").getAttribute("value"));
    cell1.innerHTML = "<input name=\"kogus" + id + "\" class=\"form-control\" type=\"number\" min=\"0\" data-toggle=\"tooltip\" title=\"Mitu ühikut ainet on vaja\" />";
    cell2.innerHTML = "<input maxlength=\"4\" name=\"ühik" + id + "\" class=\"form-control\" type=\"text\" data-toggle=\"tooltip\" title=\"Kasuta üheselt mõistetavaid ühikuid\" />";
    cell3.innerHTML = "<input name=\"aine" + id + "\" class=\"form-control\" type=\"text\" data-toggle=\"tooltip\" title=\"Aine nimi. Palun jälgi õigekirja\" />";
    document.getElementById("aineid").setAttribute("value", id+1);   
}

function showDelete(){
    $("#deleteDialogue").show();
}

function hideDelete(){
   $("#deleteDialogue").hide();
}
